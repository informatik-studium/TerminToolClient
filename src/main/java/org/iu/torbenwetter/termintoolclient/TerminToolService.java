package org.iu.torbenwetter.termintoolclient;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@EnableScheduling
@Component
class TerminToolService {

    private static final WebClient webClient = WebClient.create("http://localhost:8081/termin");

    private List<Termin> getTermine() {
        return webClient.get().uri("/list").retrieve().bodyToFlux(Termin.class).collectList().block();
    }

    private void addTermin(Termin termin) {
        webClient.post().uri("/add").body(Mono.just(termin), Termin.class).retrieve().bodyToMono(Termin.class).block();
    }

    @Scheduled(fixedRate = 5000)
    private void execute() {
        List<Termin> termine = getTermine();
        for (Termin termin : termine) {
            System.out.println(termin);
        }
    }
}
