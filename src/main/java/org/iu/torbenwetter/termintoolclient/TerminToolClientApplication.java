package org.iu.torbenwetter.termintoolclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TerminToolClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(TerminToolClientApplication.class, args);
    }

}
