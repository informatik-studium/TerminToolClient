package org.iu.torbenwetter.termintoolclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
class Termin {

    @JsonProperty("name")
    private String name;

    @JsonProperty("beginn")
    private Date beginn;

    @JsonProperty("ende")
    private Date ende;

    @Override
    public String toString() {
        return "Termin{name='" + name + "', beginn=" + beginn + ", ende=" + ende + "}";
    }
}
